package com.torrenglabs.torrot;

import java.nio.ByteBuffer;

import android.graphics.*;
import android.util.Log;
import android.view.*;


/**
 * The actual class to maintain the video stream interface with
 * the drone.
 * This class uses some native functions in order to use ffmpeg
 * library in decoding the incoming stream.
 */
public class VideoReader extends Thread {
	static {
		System.loadLibrary("torrot_video");
	}

	private Bitmap mBitmap;							/**< The Bitmap object containing decoded frame data.*/
	private ByteBuffer mByteBuffer;					/**< The buffer to hold incoming stream data.*/
	private Canvas mCanvas;							/**< The Canvas object which visualizes the video stream.*/
	
	private Boolean mFinished;						/**< The flag indicating if the stream is finished.*/
	
	public VideoUpdateListener videoUpdateListener; /**< This listener is used to update the canvas with every frame update.*/
	
	// this is set by VideoView.onSurfaceChanged
	public SurfaceHolder surfaceHolder;				/**< SurfaceHolder holding the View which is this reader's parent.*/
	
	public VideoReader()
	{
		mFinished = false;
		mBitmap = null;
		videoUpdateListener = null;
	}
	/**
	 * Starts the VideoReader thread.
	 *
	 * Establishes the connection and starts receiving frames from the drone.
	 */
	@Override
	public void run() {
		super.run();
		Log.i("com.torrenglabs.torrot", "running video thread");
		// check if we connected
		if ( connect(this, "http://192.168.1.1:5555") == 0)
		{
			// OK, start getting images
			while(!mFinished)
			{
				Log.i("com.torrenglabs.torrot", "frameframe");
				getFrame(this);
			}
		}
	}
	
	/**
	 * Finishes the decoding by changing the mFinished flag.
	 */
	public void finish()
	{
		mFinished = true;
	}
	
	/**
	 * Initializes both members, mBitmap and mByteBuffer with
	 * proper settings.
	 *
	 * @param w width of the incoming video.
	 * @param h height of the incoming video.
	 * @return initialized member mByteBuffer.
	 */
	ByteBuffer surfaceInit(int w, int h) {
		//synchronized (this) {
			Log.i("com.torrenglabs.torrot", "surfaceInit");
			
			mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
			mByteBuffer = ByteBuffer.allocateDirect(w * h * 2);
			
			return mByteBuffer;
		//}
	}
	
	/**
	 * Renders the mCanvas according to the mByteBuffer.
	 */
	void surfaceRender() {
		synchronized (surfaceHolder) {
			try {
				mCanvas = surfaceHolder.lockCanvas(null);
				Log.i("com.torrenglabs.torrot", "surfaceRenderNormalLock");
			} catch (Exception e) {
				Log.i("com.torrenglabs.torrot", "surfaceRenderExeptionCought");
			} finally
			{
				if(mCanvas != null) {
					mByteBuffer.rewind();
					mBitmap.copyPixelsFromBuffer(mByteBuffer);
					
					if (null != videoUpdateListener)
					{
						videoUpdateListener.onVideoRender(mBitmap);
					}
					Matrix M = new Matrix();
					M.setScale((float)mCanvas.getWidth()/mBitmap.getWidth(), (float)mCanvas.getHeight()/mBitmap.getHeight());
					mCanvas.setMatrix(M);
					
					mCanvas.drawBitmap(mBitmap, 0, 0, null);
					surfaceHolder.unlockCanvasAndPost(mCanvas);
					
					Log.i("com.torrenglabs.torrot", "surfaceRenderUnlockedCanvas ");				
				}
			}
		}
	}
	
	/**
	 * Recycles mBitmap and mByteBuffer for reuse in next frame.
	 */
	void surfaceRelease() {
		synchronized (this) {
			mBitmap.recycle();
			mBitmap = null;
			mByteBuffer = null;
			Log.i("com.torrenglabs.torrot", "surfaceRelease");
		}
	}
	
	/**
	 * Native function to start video connection with the drone.
	 * @param vr VideoReader object to decode the stream.
	 * @param addr The string containing the address and the port for the incoming stream.
	 */
	public native int connect(VideoReader vr, String addr);

	/**
	 * Native function to get a single frame from the stream.
	 * Called before each frame update.
	 */
	public native void getFrame(VideoReader vr);

	/**
	 * Native function to disconnect properly from the drone.
	 */
	public native void disconnect();
}
