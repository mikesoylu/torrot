package com.torrenglabs.torrot;

import java.util.Iterator;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import com.shigeodayo.ardrone.ARDrone;
import com.shigeodayo.ardrone.navdata.BatteryListener;
import com.torrenglabs.torrot.util.Clamp;
import com.torrenglabs.torrot.joystick.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;

/**
 * Main activity class
 */

public class GameActivity extends Activity {
	public ARDrone mDrone;						/**< An instance for drone object. */
	protected ColorBlobDetector mDetector; 		/**< An instance for color detector.*/
	protected boolean mIsColorSelected = false;	/**< Check for if a color is to be highlighted.*/
	protected Mat mRgba;						/**< OpenCV's matrix container. Used by openCV's image processing funcitons.*/
	protected Scalar mBlobColorRgba;			/**< Contains RGBA information of the color to be detected.*/
	protected Scalar mBlobColorHsv;				/**< Contains HSV information of the color to be detected.*/
	public VideoView mVideoView;				/**< Streamed and decoded video on screen.*/
	protected JoystickView mJoystick1;			/**< Left joystick, WASD controls.*/
	protected JoystickView mJoystick2;			/**< Right joystick, for rotating and height controls.*/

	private int speed = 45;						/**< Movement speed for the drone.*/
	private int rotateSpeed = 90;				/**< Rotation speed for the drone.*/
	private boolean droneInitialized = false;	/**< Used for initializing drone's battery listener only once.*/
	private int joystickTreshold = 2;			/**< Used for stabilizing the drone when the joystick is at center.*/
	
	/**
	 * Deprecated. Listener used to get color information of the touched region.
	 * Used for debugging purposes. Not used in final product.
	 */
	View.OnTouchListener mVideoTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int touchX = (int) event.getX();
			int touchY = (int) event.getY();
			getColor(touchX, touchY);

			return false;
		}
	};

	/**
	 * Movement listener for left joystick. 
	 */
	private JoystickMovedListener mJoystick1MovedListener = new JoystickMovedListener() {
		/**
		 * Takes user-altered joystick coordinates and sends the drone movement commands 
		 */
		@Override
		public void OnMoved(int pan, int tilt) {
			if(pan != 0)
			{
				if(pan > joystickTreshold)
					mDrone.goRight(speed);
				else if(pan < -joystickTreshold)
					mDrone.goLeft(speed);
				else
					mDrone.stop();
			}
			if(tilt != 0)
			{
				if(tilt > joystickTreshold)
					mDrone.backward(speed);
				else if(tilt < -joystickTreshold)
					mDrone.forward(speed);
				else
					mDrone.stop();
			}
		}
		/**
		 * Touchpad-release event, calls stop function to prevent unintentianel movement.
		 */
		@Override
		public void OnReleased() {
			mDrone.stop();
		}
		
		/**
		 * After the release, this event indicates that joystcik arrived to the point (0,0).
		 */
		public void OnReturnedToCenter() {
			mDrone.stop();
		};
	};
	
	/**
	 * Movement listener for right joystick. 
	 */
	private JoystickMovedListener mJoystick2MovedListener = new JoystickMovedListener() {
		
		/**
		 * Takes user-altered joystick coordinates and sends the drone movement commands 
		 */
		@Override
		public void OnMoved(int pan, int tilt) {
			if(pan != 0)
			{
				if(pan > joystickTreshold)
					mDrone.spinRight(rotateSpeed);
				else if(pan < -joystickTreshold)
					mDrone.spinLeft(rotateSpeed);
				else
					mDrone.stop();
			}
			if(tilt != 0)
			{
				if(tilt > joystickTreshold)
					mDrone.down(speed);
				else if(tilt < -joystickTreshold)
					mDrone.up(speed);
				else
					mDrone.stop();
			}
		}
		
		@Override
		public void OnReleased() {
			mDrone.stop();
		}
		
		
		@Override
		public void OnReturnedToCenter() {
			mDrone.stop();
		}
	};

	/**
	 * Computes the color at the coordinates given by parameters
	 * @param x Horizontal screen coordinate
	 * @param y Vertical screen coordinate
	 * @return false
	 */
	
	private boolean getColor(int x, int y) {
		int cols = mRgba.cols();
		int rows = mRgba.rows();

		int xOffset = (mVideoView.getWidth() - cols) / 2;
		int yOffset = (mVideoView.getHeight() - rows) / 2;

		x = (int) x - xOffset;
		y = (int) y - yOffset;

		Log.i("com.torrenglabs.torrot", "Touch image coordinates: (" + x + ", "
				+ y + ")");

		if ((x < 0) || (y < 0) || (x > cols) || (y > rows))
			return false;

		Rect touchedRect = new Rect();

		touchedRect.x = (x > 4) ? x - 4 : 0;
		touchedRect.y = (y > 4) ? y - 4 : 0;

		touchedRect.width = (x + 4 < cols) ? x + 4 - touchedRect.x : cols
				- touchedRect.x;
		touchedRect.height = (y + 4 < rows) ? y + 4 - touchedRect.y : rows
				- touchedRect.y;

		Mat touchedRegionRgba = mRgba.submat(touchedRect);

		Mat touchedRegionHsv = new Mat();
		Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv,
				Imgproc.COLOR_RGB2HSV_FULL);

		// Calculate average color of touched region
		mBlobColorHsv = Core.sumElems(touchedRegionHsv);
		int pointCount = touchedRect.width * touchedRect.height;
		for (int i = 0; i < mBlobColorHsv.val.length; i++)
			mBlobColorHsv.val[i] /= pointCount;

		mBlobColorRgba = converScalarHsv2Rgba(mBlobColorHsv);

		Log.i("com.torrenglabs.torrot", "Touched rgba color: ("
				+ mBlobColorRgba.val[0] + ", " + mBlobColorRgba.val[1] + ", "
				+ mBlobColorRgba.val[2] + ", " + mBlobColorRgba.val[3] + ")");

		mDetector.setHsvColor(mBlobColorHsv);

		mIsColorSelected = true;

		touchedRegionRgba.release();
		touchedRegionHsv.release();

		return false;
	}

	/**
	 * Listener for main application loading.
	 * 
	 */
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		/**
		 * Video initializing
		 */
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i("com.torrenglabs.torrot", "OpenCV loaded successfully");

				mVideoView = (VideoView) findViewById(R.id.das_video);

				mVideoView.setOnTouchListener(mVideoTouchListener);
				try {
					mVideoView.videoReader.start();
				} catch (Exception e) {
					Log.i("com.torrenglabs.torrot",
							"VideoThread Started Exception");
				}
				mRgba = new Mat();
				CONTOUR_COLOR = new Scalar(255, 255, 0, 255);
				mDetector = new ColorBlobDetector();
				
				// add a listener so we update our RGB mat appropriately
				mVideoView.videoReader.videoUpdateListener = new VideoUpdateListener() {
					@Override
					public void onVideoRender(Bitmap bitmap) {
						Utils.bitmapToMat(bitmap, mRgba);
						if (mIsColorSelected) {
							mDetector.process(mRgba);
							List<Point> p = mDetector.getTargets();
							for(Iterator i=p.iterator(); i.hasNext(); ) {
								Point pt = (Point)i.next();
								Clamp.clampPointToScreen(pt, new Point(mRgba.cols(), mRgba.rows()));
								Imgproc.drawContours(mRgba, mDetector.getContours(), -1, CONTOUR_COLOR);
								try {
									Mat targetBox = mRgba.submat(	(int) pt.y - 10,
																	(int) pt.y + 10,
																	(int) pt.x - 10,
																	(int) pt.x + 10);
									targetBox.setTo(CONTOUR_COLOR);
								} catch (Exception e) {
								
								}
							}
							// LAST!
							Utils.matToBitmap(mRgba, bitmap);
						}
					}
				};

			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this,
				mLoaderCallback);
	}
	/**
	 * Converts an HSV color information to a RGBA color information.
	 * @param hsvColor Scalar object that contains HSV information.
	 * @return A new Scalar object with the RGBA information.
	 */
	private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
		Mat pointMatRgba = new Mat();
		Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
		Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL,
				4);

		return new Scalar(pointMatRgba.get(0, 0));
	}
	
	/**
	 * Things to be done when application is opened.
	 * Initialize drone and interface
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_game);

		if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_2, this,
				mLoaderCallback)) {
			Log.e("TEST", "Cannot connect to OpenCV Manager");
		}

		try {
			Log.i("com.torrenglabs.torrot", "connecting");

			mDrone = new ARDrone();
			mDrone.connect();
			mDrone.connectNav();
			mDrone.start();

			mDrone.addBatteryUpdateListener(new BatteryListener() {
				@Override
				public void batteryLevelChanged(int percentage) {
					if (!droneInitialized) {
						droneInitialized = true;
						mDrone.setVideoFps(15);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		initButtons();

	}
	
	
	/**
	 * Initialize interface by getting views and setting listeners
	 */
	private void initButtons() {
		mJoystick1 = (JoystickView) findViewById(R.id.joystickView1);
		mJoystick1.setOnJostickMovedListener(mJoystick1MovedListener);
		
		mJoystick2 = (JoystickView) findViewById(R.id.joystickView2);
		mJoystick2.setOnJostickMovedListener(mJoystick2MovedListener);
		
		Button takeOff = (Button) findViewById(R.id.cmd_takeOff);
		takeOff.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN)
					mDrone.forward();
				else if (event.getAction() == MotionEvent.ACTION_UP)
					mDrone.stop();

				return true;
			}
		});
		
		Button land = (Button) findViewById(R.id.cmd_landing);
		land.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN)
					mDrone.landing();
				else if (event.getAction() == MotionEvent.ACTION_UP)
					mDrone.stop();

				return true;
			}
		});

		Button emergency = (Button) findViewById(R.id.cmd_emergency_button);
		emergency.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN)
					mDrone.reset();
				else if (event.getAction() == MotionEvent.ACTION_UP)
					mDrone.stop();

				return true;
			}
		});
		
		
		
	}
	
	/**
	 * Keydown listener. Currently only works for neatly killing the app
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			new AlertDialog.Builder(this)
					.setMessage("Upon exiting, drone will be disconnected !")
					.setTitle("Exit Torrot ?")
					.setCancelable(false)
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									// close threads & exit
									mDrone.disconnect();
									mVideoView.videoReader.finish();
									finish();
								}
							})
					.setNeutralButton(android.R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
								}
							}).show();

			return true;
		}

		return super.onKeyDown(keyCode, event);
	}
}
