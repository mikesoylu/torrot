package com.torrenglabs.torrot.util;

import org.opencv.core.Point;

public class Clamp {
	public static final void clampPointToScreen(Point point, Point bounds) {
		if(point.x <= 10)
			point.x = 10;
		if(point.x >= bounds.x - 10)
			point.x = bounds.x - 10;
		
		if(point.y <= 10)
			point.y = 10;
		if(point.y >= bounds.y - 10)
			point.y = bounds.y - 10;
	}
}
