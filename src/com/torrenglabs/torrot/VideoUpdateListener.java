package com.torrenglabs.torrot;

import android.graphics.Bitmap;

public interface VideoUpdateListener {
	public void onVideoRender(Bitmap bitmap);
}
