package com.torrenglabs.torrot.joystick;

public interface JoystickClickedListener {
	public void OnClicked();
	public void OnReleased();
}
