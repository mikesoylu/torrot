package com.torrenglabs.torrot;

import android.content.Context;
import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.util.AttributeSet;
import android.util.Log;
import android.view.*;


/**
 * Custom SurfaceView class to include ARDrone's stream decoder.
 */
public class VideoView extends SurfaceView implements Callback {

	public VideoReader videoReader;		/**< VideoReader object to decode stream data.*/
	
	/**
	 * This constructor called if View made programmatically and added with setContentView
	 * @param context Parent SurfaceView's context.
	 */
	public VideoView(Context context) {
		super(context);
		videoReader = new VideoReader();
		videoReader.surfaceHolder = getHolder();
	}
	/**
	 * This constructor called if made from XML file
	 * @param context Parent SurfaceView's context.
	 * @param attrs	Attributes specified in XML file.
	 */
	public VideoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		videoReader = new VideoReader();
		
		videoReader.surfaceHolder = getHolder();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		videoReader.surfaceHolder = holder;
		Log.i("com.torrenglabs.torrot", "surfaceChanged: updated holder");
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}
}
