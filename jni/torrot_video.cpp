#ifndef INT64_C
#define INT64_C(c) (c ## LL)
#define UINT64_C(c) (c ## ULL)
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "com_torrenglabs_torrot_VideoReader.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"

#ifdef __cplusplus
}
#endif

char              *drone_addr = "http://192.168.1.1:5555";
AVFormatContext   *pFormatCtx = NULL;
AVCodecContext    *pCodecCtx;
AVCodec           *pCodec;
AVPacket          packet;
AVFrame           *pFrame;

// converting
AVFrame           *pFrame_RGB;
uint8_t           *buffer_RGB;
struct SwsContext *pConvertCtx_RGB;

bool is_connecting = false;

static jbyte* g_buffer;

bool jni_surface_init(JNIEnv* env, jobject obj, int w, int h) {

	jclass cls = (env)->GetObjectClass(obj);
	jmethodID init = (env)->GetMethodID(cls, "surfaceInit", "(II)Ljava/nio/ByteBuffer;"); // ByteBuffer surfaceInit(int w, int h)

	jobject buf = (env)->CallObjectMethod(obj, init, w, h);
	if (buf == NULL) return false;

	g_buffer = (jbyte*)(env)->GetDirectBufferAddress(buf);
	if (g_buffer == NULL) return false;

	return true;
}

void jni_surface_release(JNIEnv* env, jobject obj) {

	jclass cls = (env)->GetObjectClass(obj);
	jmethodID release = (env)->GetMethodID(cls, "surfaceRelease", "()V");

	(env)->CallVoidMethod(obj, release);
	g_buffer = NULL;
}

void jni_surface_render(JNIEnv* env, jobject obj, uint8_t* pixels, int w, int h) {

	jclass cls = (env)->GetObjectClass(obj);
	jmethodID render = (env)->GetMethodID(cls, "surfaceRender", "()V");

	if (g_buffer != NULL) {
		memcpy(g_buffer, pixels, w * h * 2); // RGB565 pixels
		(env)->CallVoidMethod(obj, render);
	}
}

JNIEXPORT jint JNICALL Java_com_torrenglabs_torrot_VideoReader_connect(JNIEnv *env, jobject obj, jobject thiz, jstring addr = NULL)
{
	// are we already connected/trying to connect
	if (is_connecting)
		return 1;

	if (addr != NULL)
	{
		drone_addr = (char*)env->GetStringUTFChars(addr, NULL) ;
	}
	is_connecting = true;

	// register ffmpeg stuff
	av_register_all();
	avcodec_register_all();

	// open
	__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "Connecting to %s", drone_addr);
	int err;
	while((err = avformat_open_input(&pFormatCtx, drone_addr, NULL, NULL)) != 0)
	{
		char buf[128];
		av_strerror(err,buf,128);
		__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "couldn't connect:%s", buf);
	}
	// log stream data
	__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "Connected Bitcheezz\nFilename '%s', Codec %s",
            pFormatCtx->filename,
            pFormatCtx->iformat->name
        );
	int dummy = 0;


	while(dummy < 10000)
	{
		dummy++;
	}
	// stream infobus error on device
	if(!pFormatCtx)
		__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "Counted to infinity!");

	__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "FORMAT RETURNS: %d", avformat_find_stream_info(pFormatCtx, NULL));

	// get codec
	pCodecCtx = pFormatCtx->streams[0]->codec;
	pCodec    = avcodec_find_decoder(pCodecCtx->codec_id);

	__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "Got codec!");

	// init codec
	avcodec_open2(pCodecCtx, pCodec, NULL);

	__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "init'd codec");

	// convert to rgb
	pFrame_RGB = avcodec_alloc_frame();
	if(pFrame_RGB == NULL)
	{
		__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "Could not allocate pFrame_RGB!");
		return 1;
	}
	// determine required buffer size and allocate buffer
	buffer_RGB = (uint8_t *)av_malloc(avpicture_get_size(PIX_FMT_RGB565, pCodecCtx->width, pCodecCtx->height));

	// assign buffer to image planes
	avpicture_fill((AVPicture *)pFrame_RGB, buffer_RGB,
			PIX_FMT_RGB565, pCodecCtx->width, pCodecCtx->height);

	// format conversion context
	pConvertCtx_RGB = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt,
			pCodecCtx->width, pCodecCtx->height, PIX_FMT_RGB565,
			SWS_SPLINE, NULL, NULL, NULL);

	pFrame = avcodec_alloc_frame();

	__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "Calling initSurface");
	jni_surface_init(env, thiz, pCodecCtx->width, pCodecCtx->height);

	return 0;
}

JNIEXPORT jint JNICALL Java_com_torrenglabs_torrot_VideoReader_getFrame(JNIEnv *env, jobject obj, jobject thiz)
{
	int frameDecoded;
	// read frame
	if(av_read_frame(pFormatCtx, &packet) < 0)
	{
		__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "Could not read frame!");
		return 1;
	}

	// decode the frame
	if(avcodec_decode_video2(pCodecCtx, pFrame, &frameDecoded, &packet) < 0)
	{
		__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "Could not decode frame!");
		return 1;
	}

	if (frameDecoded)
	{
		__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "Frame Decoded");

		sws_scale(pConvertCtx_RGB,   (const uint8_t * const*)pFrame->data, pFrame->linesize, 0,
				pCodecCtx->height,   pFrame_RGB->data,   pFrame_RGB->linesize);

		// render the frame to java surface
		jni_surface_render(env, thiz, pFrame_RGB->data[0], pCodecCtx->width, pCodecCtx->height);

		av_free_packet(&packet);
		return 0;
	}
	av_free_packet(&packet);
	return 1;
}

JNIEXPORT void JNICALL Java_com_torrenglabs_torrot_VideoReader_disconnect(JNIEnv *env, jobject obj)
{
	__android_log_print(ANDROID_LOG_VERBOSE, "torrot_video", "disconnecting");

	// free converted
	av_free(pFrame_RGB);
	av_free(buffer_RGB);
	sws_freeContext(pConvertCtx_RGB);

	// free orig
	av_free(pFrame);
	avcodec_close(pCodecCtx);
	av_close_input_file(pFormatCtx);
}
